package graphers;

public interface IGrapher {
	// vérifie si le graphe a déjà été composé, retourne son chemin s'il existe, null sinon
	public String estDejaFait();

	// crée deux fichiers : 'callgraph.dot' contenant le code du graphe
	// et un fichier au format spécifié contenant le graphe composé, retourne son chemin
	public String terminerGraphe();

	// ajoute un noeud qui sera la source des arêtes vers les prochains noeuds destination
	public void ajouterNoeudSource(String nomPackage, String nomClasse, String nomMethode, boolean isMain);

	// ajoute un nouveau noeud qui sera la destination d'une arête depuis le dernier noeud source
	public void ajouterNoeudDestination(String nomPackage, String nomClasse, String nomMethode);

	// produit les arêtes entre le dernier source et les noeuds destination qui ont été ajoutés ensuite
	public void produireAretes();
}
