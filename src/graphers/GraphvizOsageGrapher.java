package graphers;

// l'outil qui sera utilisé pour composer le graphe est 'Graphviz'
// ce graphiste est destiné au moteur de mise en page 'osage' 
public class GraphvizOsageGrapher extends AbstractGraphvizGrapher {
	/*
	 * Attributs hérités :
	 * 
	 * - StringBuilder graphe,
	 * contient le code du graphe
	 * 
	 * - String noeudSource,
	 * nom canonique du dernier noeud source ajouté
	 * 
	 * - HashMap<String, Integer> invocations,
	 * nom de la méthode invoquée associé à son nombre d'invocations
	 */

	// calcul de marges pour aider à la lisibilité du graphe
	private int pack = 0;

	public GraphvizOsageGrapher(String idGraphe, String format) {
		super(idGraphe, format, "osage");
	}

	/*
	 * =============================================================================
	 * Résultat du graphiste
	 * =============================================================================
	 */

	// termine le graphe et le compose avec Graphviz et le moteur de mise en page 'osage'
	// retourne le chemin du fichier contenant le graphe composé
	@Override
	public String terminerGraphe() {
		// défini les marges du graphe avec la valeur calculée
		graphe.insert(0, "digraph {\n"
				+ "graph [fontname=fixed, style=rounded, forcelabels=true, splines=ortho, penwidth=10, pack=" + pack + "]\n"
				+ "node [fontname=fixed, fontsize=30, shape=box, margin=0.6, style=filled, color=forestgreen, fillcolor=palegreen]\n"
				+ "edge [fontname=fixed, fontsize=26, shape=lnormal]\n")
				.append('}');

		// réinitialisation
		pack = 0;

		return composerGraphe();
	}

	/*
	 * =============================================================================
	 * Gestion des noeuds
	 * =============================================================================
	 */

	// ajoute un noeud qui sera la source des arêtes vers les prochains noeuds destination
	@Override
	public void ajouterNoeudSource(String nomPackage, String nomClasse, String nomMethode, boolean isMain) {		
		// incrémentation de la marge pour chaque méthode déclarée
		pack++;

		super.ajouterNoeudSource(nomPackage, nomClasse, nomMethode, isMain);

		// ajout d'une arête pour démarquer la méthode 'main'
		if (isMain) {
			graphe.append("main [label=\"START\", fontsize=64, style=\"rounded\", color=red]\n"
					+ "main -> " + noeudSource + " [color=red, penwidth=12, shape=normal]\n");
		}
	}

	// ajoute le code d'un noeud au graphe
	@Override
	protected void ajouterNoeud(String nomPackage, String nomClasse, String nomMethode, String nomCanonique) {
		// le noeud est classé dans le sous-graphe de son package ainsi que de sa classe
		graphe.append("subgraph \"cluster_" + nomPackage + "\" {\n"
				+ "label=\"" + nomPackage + "\"; fontsize=50; color=orange; bgcolor=moccasin\n"
				+ "subgraph \"cluster_" + nomPackage + '.' + nomClasse + "\" {\n"
				+ "label=\"" + nomClasse + "\"; fontsize=40; color=dodgerblue; bgcolor=skyblue\n"
				+ nomCanonique + " [label=\"" + nomMethode + "\"]\n}}\n");
	}

	/*
	 * =============================================================================
	 * Gestion des arêtes
	 * =============================================================================
	 */

	// produit les arêtes entre le dernier source et les noeuds destination qui ont été ajoutés ensuite
	@Override
	public void produireAretes() {
		// incrémente la marge pour chaque méthode invoquée
		for (Integer nbInvocs : invocations.values()) {
			pack += nbInvocs;
		}

		super.produireAretes();
	}
}
