package graphers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;

public abstract class AbstractGraphvizGrapher implements IGrapher {
	private String moteur;	  // nom du moteur de mise en page
	private String format;    // format du fichier composé
	private String nomGraphe; // nom unique du graphe

	// contient le code du graphe
	protected StringBuilder graphe = new StringBuilder();

	// nom canonique du dernier noeud source ajouté
	protected String noeudSource;

	// nom de la méthode invoquée associé à son nombre d'invocations
	protected HashMap<String, Integer> invocations = new HashMap<>();

	protected AbstractGraphvizGrapher(String idGraphe, String format, String moteur) {
		this.moteur = moteur;
		this.format = format;
		nomGraphe = moteur + "_CallGraph" + idGraphe;
	}

	/*
	 * =============================================================================
	 * Résultat du graphiste
	 * =============================================================================
	 */

	// vérifie si le graphe a déjà été produit
	public String estDejaFait() {
		// si le graphe a déjà été composé
		File fichier = new File("target", nomGraphe + "." + format);
		if (fichier.exists()) {
			return fichier.getAbsolutePath();
		}

		// si le code du graphe a déjà été écrit
		fichier = new File("target", nomGraphe + ".dot");
		if (!fichier.exists()) {
			return null;
		}

		// récupération du code dans 'graphe'
		try {
			graphe = new StringBuilder(FileUtils.readFileToString(fichier));
		} catch (IOException e) {
			return null;
		}

		// composition du graphe et création du fichier
		return composerGraphe();
	}

	// utilise le moteur de mise en page en paramètre pour la composition
	// crée deux fichiers : 'callgraph.dot' contenant le code du graphe
	// et un fichier au format spécifié contenant la composition, retourne son chemin
	protected String composerGraphe() {
		// dossier qui va contenir les fichiers
		File cible = new File("target");
		cible.mkdir();

		try {
			// récupération ou création du fichier
			File fichier = new File(cible, nomGraphe + ".dot");
			fichier.createNewFile();

			// écriture du code du graphe dans le fichier
			FileUtils.writeStringToFile(fichier, graphe.toString());

			// composition du graphe déléguée au moteur de mise en page
			String cmd = moteur + " target/" + nomGraphe + ".dot -T" + format + " -o target/" + nomGraphe + "." + format;
			if (Runtime.getRuntime().exec(cmd).waitFor() != 0) {
				return "échec : code du graphe mal formé ou format inconnu";
			}
		} catch (IOException e) {
			return "échec : " + e.getMessage();
		} catch (InterruptedException e) {
			return "interrompu : " + e.getMessage();
		} finally {
			// réinitialisation du graphiste
			graphe = new StringBuilder();
		}

		return cible.getAbsolutePath() + "/" + nomGraphe + "." + format;
	}

	/*
	 * =============================================================================
	 * Gestion des noeuds
	 * =============================================================================
	 */

	// ajoute un noeud qui sera la source des arêtes vers les prochains noeuds destination
	@Override
	public void ajouterNoeudSource(String nomPackage, String nomClasse, String nomMethode, boolean isMain) {
		// nom canonique du noeud de la forme '"nomPackage.nomClasse.nomMethode"'
		noeudSource = '"' + nomPackage + '.' + nomClasse + '.' + nomMethode + '"';

		// l'écriture du code est déléguée à l'implémentation
		ajouterNoeud(nomPackage, nomClasse, nomMethode, noeudSource);
	}

	// ajoute un nouveau noeud qui sera la destination d'une arête depuis le dernier noeud source
	@Override
	public void ajouterNoeudDestination(String nomPackage, String nomClasse, String nomMethode) {
		String noeudDestination = '"' + nomPackage + '.' + nomClasse + '.' + nomMethode + '"';

		Integer cpt = invocations.get(noeudDestination);
		if (cpt == null) {
			// ajout du noeud au graphe, délégué à l'implémentation
			ajouterNoeud(nomPackage, nomClasse, nomMethode, noeudDestination);
			invocations.put(noeudDestination, 1); // première occurence
		} else {
			// incrémentation du nombre d'occurences
			invocations.put(noeudDestination, cpt + 1);
		}
	}

	// ajoute un noeud d'une manière spécifique à l'implémentation
	protected abstract void ajouterNoeud(String nomPackage, String nomClasse, String nomMethode, String nomCanonique);

	/*
	 * =============================================================================
	 * Gestion des arêtes
	 * =============================================================================
	 */

	// produit les arêtes entre le dernier source et les noeuds destination qui ont été ajoutés ensuite
	@Override
	public void produireAretes() {
		// ajoute les arêtes avec en source la méthode visitée
		// et en destination chaque méthode qu'elle a invoquée
		invocations.forEach((noeudDestination, nbInvocs) -> {
			graphe.append(noeudSource + " -> " + noeudDestination + " [headlabel=" + nbInvocs + "]\n");
		});

		// remise à zéro pour le prochain noeud source
		invocations.clear();
	}
}
