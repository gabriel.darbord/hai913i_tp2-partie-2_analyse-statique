package graphers;

// l'outil qui sera utilisé pour composer le graphe est 'Graphviz'
// ce graphiste est destiné au moteur de mise en page 'dot' 
public class GraphvizDotGrapher extends AbstractGraphvizGrapher {
	/*
	 * Attributs hérités :
	 * 
	 * - StringBuilder graphe,
	 * contient le code du graphe
	 * 
	 * - String noeudSource,
	 * nom canonique du dernier noeud source ajouté
	 * 
	 * - HashMap<String, Integer> invocations,
	 * nom de la méthode invoquée associé à son nombre d'invocations
	 */

	// calcul de marges pour aider à la lisibilité du graphe
	private int rankSep = 0; // marge entre les rangs
	
	public GraphvizDotGrapher(String idGraphe, String format) {
		super(idGraphe, format, "dot");
	}

	/*
	 * =============================================================================
	 * Résultat du graphiste
	 * =============================================================================
	 */

	// termine le graphe et le compose avec Graphviz et le moteur de mise en page 'dot'
	// retourne le chemin du fichier contenant le graphe composé
	@Override
	public String terminerGraphe() {
		// défini les marges du graphe avec la valeur calculée
		rankSep = (int) (Math.sqrt(rankSep));

		graphe.insert(0, "digraph {\n"
				+ "graph [fontname=fixed, forcelabels=true, splines=ortho, penwidth=10, pack=30, nodesep=1, ranksep=" + rankSep + "]\n"
				+ "node [fontname=fixed, fontsize=30, shape=box, margin=0.6, style=filled, color=forestgreen, fillcolor=palegreen]\n"
				+ "edge [fontname=fixed, fontsize=26, shape=lnormal]\n"
				// noeud initial du graphe, pointe sur les points d'entrée possibles du projet
				+ "main [label=START, fontsize=64, style=\"rounded\", color=red, root=true]\n"
				// noeud invisible utilisé pour éviter d'avoir des méthodes au même rang que 'main'
				+ "_ [style=invis]\n"
				+ "main -> _ [style=invis]")
				.append('}');

		// réinitialisation
		rankSep = 0;

		return composerGraphe();
	}

	/*
	 * =============================================================================
	 * Gestion des noeuds
	 * =============================================================================
	 */

	// ajoute un noeud qui sera la source des arêtes vers les prochains noeuds destination
	@Override
	public void ajouterNoeudSource(String nomPackage, String nomClasse, String nomMethode, boolean isMain) {
		super.ajouterNoeudSource(nomPackage, nomClasse, nomMethode, isMain);

		// ajout d'une arête pour démarquer la méthode 'main'
		if (isMain) {
			graphe.append("main -> " + noeudSource + " [color=red, penwidth=12, shape=normal, weight=10]\n"
					+ "{_; " + noeudSource + "}\n");
		} else {
			graphe.append("_ -> " + noeudSource + " [style=invis, weight=0]\n");
		}
	}

	// ajoute le code d'un noeud au graphe
	@Override
	protected void ajouterNoeud(String nomPackage, String nomClasse, String nomMethode, String nomCanonique) {
		// le noeud sera classé selon la hiérarchie défini par les arêtes
		graphe.append(nomCanonique + " [label=\"" + nomPackage + '.' + nomClasse + "\\n" + nomMethode + "\"]\n");
//		peut-être groupé par classe ?
//		graphe.append("subgraph \"cluster_" + nomPackage + '.' + nomClasse + "\" {\n"
//				+ "label=\"" + nomClasse + "\"; fontsize=40; color=dodgerblue; bgcolor=skyblue\n"
//				+ nomCanonique + " [label=\"" + nomMethode + "\"]\n}\n");
	}

	/*
	 * =============================================================================
	 * Gestion des arêtes
	 * =============================================================================
	 */

	// produit les arêtes entre le dernier source et les noeuds destination qui ont été ajoutés ensuite
	@Override
	public void produireAretes() {
		// incrémente la marge entre les rangs pour chaque méthode invoquée
		for (Integer nbInvocs : invocations.values()) {
			rankSep += nbInvocs;
		}

		super.produireAretes();
	}
}
