package ui;

import analyzer.ASTProcessor;

public abstract class AbstractUserInterface {
	protected static final String GRAPHE_FORMAT = "pdf";

	protected final ASTProcessor analyseur = new ASTProcessor();

	protected abstract void afficherMetriques();

	protected abstract void afficherGrapheAppel();
}
