package ui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;

import main.Utils;

import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionListener;

import javax.swing.event.ListSelectionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.JRadioButton;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class GraphicalUserInterface extends AbstractUserInterface {

	private JFrame frame;

	// nombres
	private JLabel lblNbClasses;
	private JLabel lblNbLignes;
	private JLabel lblNbMethodes;
	private JLabel lblNbPackages;
	private JLabel lblMoyMethodes;
	private JLabel lblMoyLignes;
	private JLabel lblMoyAttributs;
	private JLabel lblNbMaxParams;

	// top 10%
	private DefaultListModel<String> modelTopClassesMethodes;
	private DefaultListModel<String> modelTopClassesAttributs;
	private DefaultListModel<String> modelTopClassesInter;
	private DefaultListModel<String> modelTopMethodes_Classes;
	private DefaultListModel<String> modelTopMethodes_Methodes;
	private ArrayList<ArrayList<String>> topMethodesParClasse;

	// plus de X
	private JSpinner spinnerX;
	private DefaultListModel<String> modelClassesXMethodes;

	// graphe d'appel
	private boolean avecHierarchie = true;
	private String cheminGraphe;
//	private JLabel lblGraphe;
//	private JWebBrowser webBrowser;

	/**
	 * Create the application.
	 */
	public GraphicalUserInterface() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1200, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton btnChoisirUnProjet = new JButton("Sélectionner un projet...");
		btnChoisirUnProjet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				// sélecteur de fichier pour le projet à analyser
				final JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				final JFrame fcFrame = new JFrame();

				int val = fc.showOpenDialog(fcFrame);

				if (val != JFileChooser.APPROVE_OPTION)
					return;

				String projectPath = fc.getSelectedFile().getPath();

				// détournez le regard de cette classe anonyme et tout ira bien
				new SwingWorker<Void, Void>() {
					// pas de constructeur alors on fait ça...
					@SuppressWarnings("unused")
					Object a = new Object() {
						{
							frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						}
					};

					@Override
					protected Void doInBackground() throws Exception {
						try {
							analyseur.analyser(projectPath);
						} catch (IOException e) {
							JOptionPane.showMessageDialog(new JFrame(), e.getMessage());
							return null;
						}
						afficherMetriques();
						afficherGrapheAppel();
						return null;
					}

					@Override
					protected void done() {
						frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					}
				}// SwingWorker
				.execute();
			}
		});
		frame.getContentPane().add(btnChoisirUnProjet, BorderLayout.NORTH);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Métriques", null, panel, null);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 300, 603, 0 };
		gbl_panel.rowHeights = new int[] { 350, 300 };
		gbl_panel.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 1.0 };
		panel.setLayout(gbl_panel);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(BorderFactory.createCompoundBorder(
				new TitledBorder(null, "Des nombres", TitledBorder.LEADING, TitledBorder.TOP, null, null),
				new EmptyBorder(10, 10, 10, 10)));
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.insets = new Insets(0, 0, 5, 5);
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		panel.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 300, 50, 50 };
		gbl_panel_2.rowHeights = new int[] { 42, 42, 42, 42, 42, 42, 42, 42 };
		gbl_panel_2.columnWeights = new double[] { 0.7, 0.2, 0.1 };
		gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		panel_2.setLayout(gbl_panel_2);

		JLabel lblClasses = new JLabel();
		lblClasses.setText("Classes :");
		GridBagConstraints gbc_lblClasses = new GridBagConstraints();
		gbc_lblClasses.fill = GridBagConstraints.BOTH;
		gbc_lblClasses.insets = new Insets(0, 0, 5, 5);
		gbc_lblClasses.gridx = 0;
		gbc_lblClasses.gridy = 0;
		panel_2.add(lblClasses, gbc_lblClasses);

		lblNbClasses = new JLabel();
		GridBagConstraints gbc_lblNbClasses = new GridBagConstraints();
		gbc_lblNbClasses.anchor = GridBagConstraints.EAST;
		gbc_lblNbClasses.fill = GridBagConstraints.VERTICAL;
		gbc_lblNbClasses.insets = new Insets(0, 0, 5, 0);
		gbc_lblNbClasses.gridx = 1;
		gbc_lblNbClasses.gridy = 0;
		panel_2.add(lblNbClasses, gbc_lblNbClasses);

		JLabel lblLignes = new JLabel();
		lblLignes.setText("Lignes :");
		GridBagConstraints gbc_lblLignes = new GridBagConstraints();
		gbc_lblLignes.fill = GridBagConstraints.BOTH;
		gbc_lblLignes.insets = new Insets(0, 0, 5, 5);
		gbc_lblLignes.gridx = 0;
		gbc_lblLignes.gridy = 1;
		panel_2.add(lblLignes, gbc_lblLignes);

		lblNbLignes = new JLabel();
		GridBagConstraints gbc_lblNbLignes = new GridBagConstraints();
		gbc_lblNbLignes.anchor = GridBagConstraints.EAST;
		gbc_lblNbLignes.fill = GridBagConstraints.VERTICAL;
		gbc_lblNbLignes.insets = new Insets(0, 0, 5, 0);
		gbc_lblNbLignes.gridx = 1;
		gbc_lblNbLignes.gridy = 1;
		panel_2.add(lblNbLignes, gbc_lblNbLignes);

		JLabel lblMethodes = new JLabel();
		lblMethodes.setText("Méthodes :");
		GridBagConstraints gbc_lblMethodes = new GridBagConstraints();
		gbc_lblMethodes.fill = GridBagConstraints.BOTH;
		gbc_lblMethodes.insets = new Insets(0, 0, 5, 5);
		gbc_lblMethodes.gridx = 0;
		gbc_lblMethodes.gridy = 2;
		panel_2.add(lblMethodes, gbc_lblMethodes);

		lblNbMethodes = new JLabel();
		GridBagConstraints gbc_lblNbMethodes = new GridBagConstraints();
		gbc_lblNbMethodes.anchor = GridBagConstraints.EAST;
		gbc_lblNbMethodes.fill = GridBagConstraints.VERTICAL;
		gbc_lblNbMethodes.insets = new Insets(0, 0, 5, 0);
		gbc_lblNbMethodes.gridx = 1;
		gbc_lblNbMethodes.gridy = 2;
		panel_2.add(lblNbMethodes, gbc_lblNbMethodes);

		JLabel lblPackages = new JLabel();
		lblPackages.setText("Packages :");
		GridBagConstraints gbc_lblPackages = new GridBagConstraints();
		gbc_lblPackages.fill = GridBagConstraints.BOTH;
		gbc_lblPackages.insets = new Insets(0, 0, 5, 5);
		gbc_lblPackages.gridx = 0;
		gbc_lblPackages.gridy = 3;
		panel_2.add(lblPackages, gbc_lblPackages);

		lblNbPackages = new JLabel();
		GridBagConstraints gbc_lblNbPackages = new GridBagConstraints();
		gbc_lblNbPackages.anchor = GridBagConstraints.EAST;
		gbc_lblNbPackages.fill = GridBagConstraints.VERTICAL;
		gbc_lblNbPackages.insets = new Insets(0, 0, 5, 0);
		gbc_lblNbPackages.gridx = 1;
		gbc_lblNbPackages.gridy = 3;
		panel_2.add(lblNbPackages, gbc_lblNbPackages);

		JLabel lblNombreMoyenDe = new JLabel();
		lblNombreMoyenDe.setText("Nombre moyen de méthodes par classe :");
		GridBagConstraints gbc_lblNombreMoyenDe = new GridBagConstraints();
		gbc_lblNombreMoyenDe.fill = GridBagConstraints.BOTH;
		gbc_lblNombreMoyenDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreMoyenDe.gridx = 0;
		gbc_lblNombreMoyenDe.gridy = 4;
		panel_2.add(lblNombreMoyenDe, gbc_lblNombreMoyenDe);

		lblMoyMethodes = new JLabel();
		GridBagConstraints gbc_lblMoyMethodes = new GridBagConstraints();
		gbc_lblMoyMethodes.anchor = GridBagConstraints.EAST;
		gbc_lblMoyMethodes.fill = GridBagConstraints.VERTICAL;
		gbc_lblMoyMethodes.insets = new Insets(0, 0, 5, 0);
		gbc_lblMoyMethodes.gridx = 1;
		gbc_lblMoyMethodes.gridy = 4;
		panel_2.add(lblMoyMethodes, gbc_lblMoyMethodes);

		JLabel lblNombreMoyenDe_1 = new JLabel();
		lblNombreMoyenDe_1.setText("Nombre moyen de lignes de code par méthode :");
		GridBagConstraints gbc_lblNombreMoyenDe_1 = new GridBagConstraints();
		gbc_lblNombreMoyenDe_1.fill = GridBagConstraints.BOTH;
		gbc_lblNombreMoyenDe_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreMoyenDe_1.gridx = 0;
		gbc_lblNombreMoyenDe_1.gridy = 5;
		panel_2.add(lblNombreMoyenDe_1, gbc_lblNombreMoyenDe_1);

		lblMoyLignes = new JLabel();
		GridBagConstraints gbc_lblMoyLignes = new GridBagConstraints();
		gbc_lblMoyLignes.anchor = GridBagConstraints.EAST;
		gbc_lblMoyLignes.fill = GridBagConstraints.VERTICAL;
		gbc_lblMoyLignes.insets = new Insets(0, 0, 5, 0);
		gbc_lblMoyLignes.gridx = 1;
		gbc_lblMoyLignes.gridy = 5;
		panel_2.add(lblMoyLignes, gbc_lblMoyLignes);

		JLabel lblNombreMoyenDattributs = new JLabel();
		lblNombreMoyenDattributs.setText("Nombre moyen d’attributs par classe :");
		GridBagConstraints gbc_lblNombreMoyenDattributs = new GridBagConstraints();
		gbc_lblNombreMoyenDattributs.fill = GridBagConstraints.BOTH;
		gbc_lblNombreMoyenDattributs.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreMoyenDattributs.gridx = 0;
		gbc_lblNombreMoyenDattributs.gridy = 6;
		panel_2.add(lblNombreMoyenDattributs, gbc_lblNombreMoyenDattributs);

		lblMoyAttributs = new JLabel();
		GridBagConstraints gbc_lblMoyAttributs = new GridBagConstraints();
		gbc_lblMoyAttributs.anchor = GridBagConstraints.EAST;
		gbc_lblMoyAttributs.insets = new Insets(0, 0, 5, 0);
		gbc_lblMoyAttributs.fill = GridBagConstraints.VERTICAL;
		gbc_lblMoyAttributs.gridx = 1;
		gbc_lblMoyAttributs.gridy = 6;
		panel_2.add(lblMoyAttributs, gbc_lblMoyAttributs);

		JLabel lblNombreMaximalDe = new JLabel();
		lblNombreMaximalDe.setText("nombre maximal de paramètres :");
		GridBagConstraints gbc_lblNombreMaximalDe = new GridBagConstraints();
		gbc_lblNombreMaximalDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreMaximalDe.fill = GridBagConstraints.BOTH;
		gbc_lblNombreMaximalDe.gridx = 0;
		gbc_lblNombreMaximalDe.gridy = 7;
		panel_2.add(lblNombreMaximalDe, gbc_lblNombreMaximalDe);

		lblNbMaxParams = new JLabel();
		GridBagConstraints gbc_lblNbMaxParams = new GridBagConstraints();
		gbc_lblNbMaxParams.anchor = GridBagConstraints.EAST;
		gbc_lblNbMaxParams.insets = new Insets(0, 0, 5, 0);
		gbc_lblNbMaxParams.gridx = 1;
		gbc_lblNbMaxParams.gridy = 7;
		panel_2.add(lblNbMaxParams, gbc_lblNbMaxParams);

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(BorderFactory.createCompoundBorder(
				new TitledBorder(null, "Top 10%", TitledBorder.LEADING, TitledBorder.TOP, null, null),
				new EmptyBorder(5, 10, 10, 10)));
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.gridheight = 2;
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 1;
		gbc_panel_3.gridy = 0;
		panel.add(panel_3, gbc_panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 300 };
		gbl_panel_3.rowHeights = new int[] { 10, 100, 10, 100, 10, 100, 10, 100 };
		gbl_panel_3.columnWeights = new double[] { 1.0 };
		gbl_panel_3.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0 };
		panel_3.setLayout(gbl_panel_3);

		JLabel lblDesClasses = new JLabel();
		lblDesClasses.setText("Classes qui possèdent le plus grand nombre de méthodes :");
		GridBagConstraints gbc_lblDesClasses = new GridBagConstraints();
		gbc_lblDesClasses.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblDesClasses.insets = new Insets(0, 0, 5, 0);
		gbc_lblDesClasses.gridx = 0;
		gbc_lblDesClasses.gridy = 0;
		panel_3.add(lblDesClasses, gbc_lblDesClasses);

		modelTopClassesMethodes = new DefaultListModel<>();
		JList<?> listTopClassesMethodes = new JList<>(modelTopClassesMethodes);
		GridBagConstraints gbc_listTopClassesMethodes = new GridBagConstraints();
		gbc_listTopClassesMethodes.insets = new Insets(0, 0, 5, 0);
		gbc_listTopClassesMethodes.fill = GridBagConstraints.BOTH;
		gbc_listTopClassesMethodes.gridx = 0;
		gbc_listTopClassesMethodes.gridy = 1;
		panel_3.add(new JScrollPane(listTopClassesMethodes), gbc_listTopClassesMethodes);

		JLabel lblClassesQuiPossdent = new JLabel("Classes qui possèdent le plus grand nombre d’attributs :");
		GridBagConstraints gbc_lblClassesQuiPossdent = new GridBagConstraints();
		gbc_lblClassesQuiPossdent.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblClassesQuiPossdent.insets = new Insets(0, 0, 5, 0);
		gbc_lblClassesQuiPossdent.gridx = 0;
		gbc_lblClassesQuiPossdent.gridy = 2;
		panel_3.add(lblClassesQuiPossdent, gbc_lblClassesQuiPossdent);

		modelTopClassesAttributs = new DefaultListModel<>();
		JList<?> listTopClassesAttributs = new JList<>(modelTopClassesAttributs);
		GridBagConstraints gbc_listTopClassesAttributs = new GridBagConstraints();
		gbc_listTopClassesAttributs.insets = new Insets(0, 0, 5, 0);
		gbc_listTopClassesAttributs.fill = GridBagConstraints.BOTH;
		gbc_listTopClassesAttributs.gridx = 0;
		gbc_listTopClassesAttributs.gridy = 3;
		panel_3.add(new JScrollPane(listTopClassesAttributs), gbc_listTopClassesAttributs);

		JLabel lblClassesCommunesAux = new JLabel("Classes communes aux catégories précédentes :");
		GridBagConstraints gbc_lblClassesCommunesAux = new GridBagConstraints();
		gbc_lblClassesCommunesAux.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblClassesCommunesAux.insets = new Insets(0, 0, 5, 0);
		gbc_lblClassesCommunesAux.gridx = 0;
		gbc_lblClassesCommunesAux.gridy = 4;
		panel_3.add(lblClassesCommunesAux, gbc_lblClassesCommunesAux);

		modelTopClassesInter = new DefaultListModel<>();
		JList<?> listTopClassesInter = new JList<>(modelTopClassesInter);
		GridBagConstraints gbc_listTopClassesInter = new GridBagConstraints();
		gbc_listTopClassesInter.insets = new Insets(0, 0, 5, 0);
		gbc_listTopClassesInter.fill = GridBagConstraints.BOTH;
		gbc_listTopClassesInter.gridx = 0;
		gbc_listTopClassesInter.gridy = 5;
		panel_3.add(new JScrollPane(listTopClassesInter), gbc_listTopClassesInter);

		JLabel lblMthodesQuiPossdent = new JLabel(
				"Méthodes qui possèdent le plus grand nombre de lignes de code par classe :");
		GridBagConstraints gbc_lblMthodesQuiPossdent = new GridBagConstraints();
		gbc_lblMthodesQuiPossdent.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblMthodesQuiPossdent.insets = new Insets(0, 0, 5, 0);
		gbc_lblMthodesQuiPossdent.gridx = 0;
		gbc_lblMthodesQuiPossdent.gridy = 6;
		panel_3.add(lblMthodesQuiPossdent, gbc_lblMthodesQuiPossdent);

		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 7;
		panel_3.add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 300, 300 };
		gbl_panel_4.rowHeights = new int[] { 100 };
		gbl_panel_4.columnWeights = new double[] { 0.0, 0.0 };
		gbl_panel_4.rowWeights = new double[] { 1.0 };
		panel_4.setLayout(gbl_panel_4);

		modelTopMethodes_Classes = new DefaultListModel<>();
		JList<?> listTopMethodes_Classes = new JList<>(modelTopMethodes_Classes);
		listTopMethodes_Classes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listTopMethodes_Classes.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int index = listTopMethodes_Classes.getSelectedIndex();
				modelTopMethodes_Methodes.clear();
				modelTopMethodes_Methodes.addAll(topMethodesParClasse.get(index));
			}
		});
		GridBagConstraints gbc_listTopMethodes_Classes = new GridBagConstraints();
		gbc_listTopMethodes_Classes.fill = GridBagConstraints.BOTH;
		gbc_listTopMethodes_Classes.insets = new Insets(0, 0, 0, 5);
		gbc_listTopMethodes_Classes.gridx = 0;
		gbc_listTopMethodes_Classes.gridy = 0;
		panel_4.add(new JScrollPane(listTopMethodes_Classes), gbc_listTopMethodes_Classes);

		modelTopMethodes_Methodes = new DefaultListModel<>();
		JList<?> listTopMethodes_Methodes = new JList<>(modelTopMethodes_Methodes);
		GridBagConstraints gbc_listTopMethodes_Methodes = new GridBagConstraints();
		gbc_listTopMethodes_Methodes.fill = GridBagConstraints.BOTH;
		gbc_listTopMethodes_Methodes.gridx = 1;
		gbc_listTopMethodes_Methodes.gridy = 0;
		panel_4.add(new JScrollPane(listTopMethodes_Methodes), gbc_listTopMethodes_Methodes);

		JPanel panel_6 = new JPanel();
		panel_6.setBorder(BorderFactory.createCompoundBorder(
				new TitledBorder(null, "Plus de X", TitledBorder.LEADING, TitledBorder.TOP, null, null),
				new EmptyBorder(10, 10, 10, 10)));
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.insets = new Insets(0, 0, 0, 5);
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 1;
		panel.add(panel_6, gbc_panel_6);
		GridBagLayout gbl_panel_6 = new GridBagLayout();
		gbl_panel_6.columnWidths = new int[] { 1 };
		gbl_panel_6.rowHeights = new int[] { 15, 175 };
		gbl_panel_6.columnWeights = new double[] { 1.0 };
		gbl_panel_6.rowWeights = new double[] { 0.0, 0.0 };
		panel_6.setLayout(gbl_panel_6);

		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel_5.insets = new Insets(0, 0, 0, 5);
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 0;
		panel_6.add(panel_5, gbc_panel_5);
		GridBagLayout gbl_panel_5 = new GridBagLayout();
		gbl_panel_5.columnWidths = new int[] { 0 };
		gbl_panel_5.rowHeights = new int[] { 15, 0 };
		gbl_panel_5.columnWeights = new double[] { 0.0, 0.0, 0.0 };
		gbl_panel_5.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panel_5.setLayout(gbl_panel_5);

		JLabel lblClassesQuiPossdent_1 = new JLabel("Classes qui possèdent plus de");
		GridBagConstraints gbc_lblClassesQuiPossdent_1 = new GridBagConstraints();
		gbc_lblClassesQuiPossdent_1.insets = new Insets(0, 0, 0, 5);
		gbc_lblClassesQuiPossdent_1.gridx = 0;
		gbc_lblClassesQuiPossdent_1.gridy = 0;
		panel_5.add(lblClassesQuiPossdent_1, gbc_lblClassesQuiPossdent_1);

		spinnerX = new JSpinner(new SpinnerNumberModel(0, 0, null, 1));
		spinnerX.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int nbMethodesSelecteur = (int) spinnerX.getValue();
				modelClassesXMethodes.clear();
				modelClassesXMethodes.addAll(analyseur.classesAvecXMethodes(nbMethodesSelecteur));
			}
		});
		GridBagConstraints gbc_spinnerX = new GridBagConstraints();
		gbc_spinnerX.insets = new Insets(0, 0, 0, 5);
		gbc_spinnerX.gridx = 1;
		gbc_spinnerX.gridy = 0;
		panel_5.add(spinnerX, gbc_spinnerX);

		JLabel lblMthodes = new JLabel("méthodes :");
		GridBagConstraints gbc_lblMthodes = new GridBagConstraints();
		gbc_lblMthodes.gridx = 2;
		gbc_lblMthodes.gridy = 0;
		panel_5.add(lblMthodes, gbc_lblMthodes);

		modelClassesXMethodes = new DefaultListModel<>();
		JList<?> listClassesXMethodes = new JList<>(modelClassesXMethodes);
		GridBagConstraints gbc_listClassesXMethodes = new GridBagConstraints();
		gbc_listClassesXMethodes.fill = GridBagConstraints.BOTH;
		gbc_listClassesXMethodes.gridx = 0;
		gbc_listClassesXMethodes.gridy = 1;
		panel_6.add(new JScrollPane(listClassesXMethodes), gbc_listClassesXMethodes);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Graphe d'appel", null, panel_1, null);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 10, 500 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 0.0, 0.0 };
		gbl_panel_1.rowWeights = new double[] { 0.1, 0.9 };
		panel_1.setLayout(gbl_panel_1);

		JRadioButton rdbtnVueHirarchique = new JRadioButton("Vue hiérarchique");
		rdbtnVueHirarchique.setSelected(true);
		rdbtnVueHirarchique.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					avecHierarchie = true;
				} else if (e.getStateChange() == ItemEvent.DESELECTED) {
					avecHierarchie = false;
				} else {
					return;
				}

				new SwingWorker<Void, Void>() {
					@SuppressWarnings("unused")
					Object a = new Object() {
						{
							frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						}
					};

					@Override
					protected Void doInBackground() throws Exception {
						afficherGrapheAppel();
						return null;
					}

					@Override
					protected void done() {
						frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					}
				}// SwingWorker
				.execute();
			}
		});
		GridBagConstraints gbc_rdbtnVueHirarchique = new GridBagConstraints();
		gbc_rdbtnVueHirarchique.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnVueHirarchique.gridx = 0;
		gbc_rdbtnVueHirarchique.gridy = 0;
		panel_1.add(rdbtnVueHirarchique, gbc_rdbtnVueHirarchique);

		JRadioButton rdbtnVueCartographique = new JRadioButton("Vue cartographique");
		GridBagConstraints gbc_rdbtnVueCartographique = new GridBagConstraints();
		gbc_rdbtnVueCartographique.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnVueCartographique.gridx = 1;
		gbc_rdbtnVueCartographique.gridy = 0;
		panel_1.add(rdbtnVueCartographique, gbc_rdbtnVueCartographique);

		ButtonGroup rdbtnGroup = new ButtonGroup();
		rdbtnGroup.add(rdbtnVueHirarchique);
		rdbtnGroup.add(rdbtnVueCartographique);

		JButton btnOuvrirLemplacementDu = new JButton("Ouvrir l'emplacement du graphe");
		btnOuvrirLemplacementDu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (cheminGraphe != null && Desktop.isDesktopSupported()) {
					File cible = new File(cheminGraphe);
					if (!cible.exists()) {
						JOptionPane.showMessageDialog(new JFrame(), "Le fichier " + cible + " n'existe pas.");
						return;
					}
					cible = cible.getParentFile();
					ouvrirFichier(cible);
				}
			}
		});
		GridBagConstraints gbc_btnOuvrirLemplacementDu = new GridBagConstraints();
		gbc_btnOuvrirLemplacementDu.insets = new Insets(0, 0, 5, 0);
		gbc_btnOuvrirLemplacementDu.gridx = 2;
		gbc_btnOuvrirLemplacementDu.gridy = 0;
		panel_1.add(btnOuvrirLemplacementDu, gbc_btnOuvrirLemplacementDu);

		JButton btnAfficherLeGraphe = new JButton("Afficher  le graphe avec l'application par défaut");
		btnAfficherLeGraphe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (cheminGraphe != null && Desktop.isDesktopSupported()) {
					File cible = new File(cheminGraphe);
					if (!cible.exists()) {
						JOptionPane.showMessageDialog(new JFrame(), "Le fichier " + cible + " n'existe pas.");
						return;
					}
					ouvrirFichier(cible);
				}
			}
		});
		btnAfficherLeGraphe.setToolTipText("Nous n'avons réussi à afficher le graphe dans cette fenêtre.");
		GridBagConstraints gbc_btnAfficherLeGraphe = new GridBagConstraints();
		gbc_btnAfficherLeGraphe.insets = new Insets(0, 0, 0, 5);
		gbc_btnAfficherLeGraphe.gridx = 0;
		gbc_btnAfficherLeGraphe.gridy = 1;
		gbc_btnAfficherLeGraphe.gridwidth = 3;
		panel_1.add(btnAfficherLeGraphe, gbc_btnAfficherLeGraphe);

		/*
		 * tentatives infructueuses d'insérer une image du graphe généré
		 */
		// image dans un label
//		lblGraphe = new JLabel();
//		GridBagConstraints gbc_lblGraphe = new GridBagConstraints();
//		gbc_lblGraphe.gridx = 0;
//		gbc_lblGraphe.gridy = 1;
//		gbc_lblGraphe.gridwidth = 3;
//		panel_1.add(lblGraphe, gbc_lblGraphe);

		// pdf dans un navigateur embarqué avec DJNativeSwing
//		JPanel panelGraphe = new JPanel();
//		GridBagConstraints gbc_panelGraphe = new GridBagConstraints();
//		gbc_panelGraphe.fill = GridBagConstraints.BOTH;
//		gbc_panelGraphe.gridx = 0;
//		gbc_panelGraphe.gridy = 1;
//		gbc_panelGraphe.gridwidth = 3;
//		panel_1.add(panelGraphe, gbc_panelGraphe);
//		
//		webBrowser = new JWebBrowser();
//		webBrowser.setBarsVisible(false);
//		webBrowser.setStatusBarVisible(false);
//		webBrowser.setVisible(true);
//		panelGraphe.add(webBrowser, BorderLayout.CENTER);
	}

	@Override
	protected void afficherMetriques() {
		// des nombres
		lblNbClasses.setText("" + analyseur.nbClasses());
		lblNbLignes.setText("" + analyseur.nbLignes());
		lblNbMethodes.setText("" + analyseur.nbMethodes());
		lblNbPackages.setText("" + analyseur.nbPackages());
		lblMoyMethodes.setText("" + analyseur.moyenneMethodesParClasse());
		lblMoyLignes.setText("" + analyseur.moyenneLignesParMethode());
		lblMoyAttributs.setText("" + analyseur.moyenneAttributsParClasse());
		lblNbMaxParams.setText("" + analyseur.nbMaxParametres());

		// top 10%
		ArrayList<String> topClassesMethodes = analyseur.topClassesParNbMethodes();
		ArrayList<String> topClassesAttributs = analyseur.topClassesParNbAttributs();
		ArrayList<String> inter = Utils.intersection(topClassesMethodes, topClassesAttributs);

		modelTopClassesMethodes.clear();
		modelTopClassesMethodes.addAll(topClassesMethodes);
		modelTopClassesAttributs.clear();
		modelTopClassesAttributs.addAll(topClassesAttributs);
		modelTopClassesInter.clear();
		modelTopClassesInter.addAll(inter);

		modelTopMethodes_Classes.clear();
		topMethodesParClasse = new ArrayList<>();
		analyseur.topMethodesParNbLignes().forEach((classe, methodes) -> {
			modelTopMethodes_Classes.addElement(classe);
			topMethodesParClasse.add(methodes);
		});

		// plus de X
		int nbMethodesSelecteur = (int) spinnerX.getValue();
		modelClassesXMethodes.clear();
		modelClassesXMethodes.addAll(analyseur.classesAvecXMethodes(nbMethodesSelecteur));
	}

	@Override
	protected void afficherGrapheAppel() {
		cheminGraphe = analyseur.grapheAppel(avecHierarchie, GRAPHE_FORMAT);
		File fichier = new File(cheminGraphe);

		if (!fichier.exists()) {
			JOptionPane.showMessageDialog(new JFrame(), "Le graphe n'a pas pu être généré.");
			cheminGraphe = null;
			return;
		}

		/*
		 * tentatives infructueuses d'insérer une image du graphe généré
		 */
		// image dans un label
//		ImageIcon graphe;
//		try {
//			graphe = new ImageIcon(Files.readAllBytes(fichier.toPath()));
//		} catch (IOException e) {
//			JOptionPane.showMessageDialog(new JFrame(), e.getMessage());
//			return;
//		}
//		lblGraphe.setIcon(graphe);

		// pdf dans navigateur embarqué
//		webBrowser.navigate(fichier.getAbsolutePath());
	}

	private void ouvrirFichier(File cible) {
		try {
			Desktop.getDesktop().open(cible);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(new JFrame(),
					"Impossible d'ouvrir le fichier " + cible + " : " + e.getMessage());
		}
	}
}
