package ui;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import main.Utils;

public class CommandLineInterface extends AbstractUserInterface {
	private int nbMethodesSelecteur;

	public CommandLineInterface(String projectPath, int nbMethodesSelecteur) {
		this.nbMethodesSelecteur = nbMethodesSelecteur;

		System.out.println("=== Projet : " + projectPath + "\n= Analyse en cours...");
		try {
			analyseur.analyser(projectPath);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return;
		}

		System.out.println("= Analyse terminée !\n");
		afficherMetriques();

		System.out.println("\n= Construction du graphe d'appel...");
		afficherGrapheAppel();
	}

	protected void afficherMetriques() {
		ArrayList<String> topClassesMethodes = analyseur.topClassesParNbMethodes();
		ArrayList<String> topClassesAttributs = analyseur.topClassesParNbAttributs();
		ArrayList<String> inter = Utils.intersection(topClassesMethodes, topClassesAttributs);
		String ln = System.lineSeparator();

		// affichage des métriques obtenues
		System.out.println("1. Nombre de classes de l’application : " + analyseur.nbClasses() + ln
				+ "2. Nombre de lignes de code de l’application : " + analyseur.nbLignes() + ln
				+ "3. Nombre total de méthodes de l’application : " + analyseur.nbMethodes() + ln
				+ "4. Nombre total de packages de l’application : " + analyseur.nbPackages() + ln
				+ "5. Nombre moyen de méthodes par classe : " + analyseur.moyenneMethodesParClasse() + ln
				+ "6. Nombre moyen de lignes de code par méthode : " + analyseur.moyenneLignesParMethode() + ln
				+ "7. Nombre moyen d'attributs par classe : " + analyseur.moyenneAttributsParClasse() + ln
				+ "8. Les 10% des classes qui possèdent le plus grand nombre de méthodes : " + topClassesMethodes + ln
				+ "9. Les 10% des classes qui possèdent le plus grand nombre d'attributs : " + topClassesAttributs + ln
				+ "10. Les classes qui font partie en même temps des deux catégories précédentes : " + inter + ln
				+ "11. Les classes qui possèdent plus de X=" + nbMethodesSelecteur + " méthodes : "
				+ analyseur.classesAvecXMethodes(nbMethodesSelecteur) + ln
				+ "12. Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code (par classe) : "
				+ analyseur.topMethodesParNbLignes() + ln
				+ "13. Le nombre maximal de paramètres par rapport à toutes les méthodes de l’application : "
				+ analyseur.nbMaxParametres());
	}

	protected void afficherGrapheAppel() {
		String chemin = analyseur.grapheAppel(true, GRAPHE_FORMAT);
		System.out.println("= Graphe d'appel terminé : " + chemin);

		// ouverture du fichier
		if (Desktop.isDesktopSupported()) {
			File graphe = new File(chemin);
			try {
				Desktop.getDesktop().open(graphe);
			} catch (IOException e) {
				System.err.println("Impossible d'ouvrir le fichier " + graphe + " : " + e.getMessage());
			}
		}
	}
}
