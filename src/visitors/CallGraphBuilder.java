package visitors;

import java.lang.reflect.Modifier;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.PrimitiveType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import graphers.IGrapher;

public class CallGraphBuilder extends ASTVisitor {
	// s'occupe de construire le code du graphe à partir des éléments visités
	private IGrapher graphiste;

	private String nomPackage; // dernier package visité
	private String nomClasse;  // dernière classe visitée
	private String nomMethode; // dernière méthode visitée

	// injection du graphiste
	public CallGraphBuilder(IGrapher grapher) {
		graphiste = grapher;
	}

	/*
	 * =============================================================================
	 * Visites
	 * =============================================================================
	 */

	// déclaration de package
	public boolean visit(PackageDeclaration node) {
		nomPackage = node.getName().getFullyQualifiedName();
		return true;
	}

	// déclaration de classe
	public boolean visit(TypeDeclaration node) {
		nomClasse = node.getName().getIdentifier();
		return true;
	}

	// déclaration de méthode
	public boolean visit(MethodDeclaration node) {
		// calcul de la signature de la méthode appelante
		nomMethode = node.getName().getIdentifier();
		List<?> parameters = node.parameters();

		nomMethode += '(';
		for (int i = 0; i < parameters.size(); i++) {
			// le premier paramètre peut être 'this' ce qui n'est pas une déclaration
			if (i == 0 && !(parameters.get(0) instanceof SingleVariableDeclaration)) {
				nomMethode += "this";
			} else {
				SingleVariableDeclaration varParam = (SingleVariableDeclaration) parameters.get(i);
				nomMethode += varParam.getType().toString();
			}

			if (i + 1 < parameters.size()) {
				nomMethode += ',';
			}
		}
		nomMethode += ')';

		// si la méthode visitée est un point d'entrée du programme
		boolean isMain = nomMethode.contentEquals("main(String[])")
				&& (node.getModifiers() & Modifier.PUBLIC) != 0
				&& (node.getModifiers() & Modifier.STATIC) != 0
				&& node.getReturnType2() != null
				&& node.getReturnType2().isPrimitiveType()
				&& ((PrimitiveType) node.getReturnType2()).getPrimitiveTypeCode() == PrimitiveType.VOID;

		// ajout du noeud au graphe
		graphiste.ajouterNoeudSource(nomPackage, nomClasse, nomMethode, isMain);

		return true;
	}

	// après chaque déclaration de méthode
	public void endVisit(MethodDeclaration node) {
		// produit les arêtes de la méthode déclarée vers ses méthodes invoquées
		graphiste.produireAretes();
	}

	/*
	 * une invocation de méthode prend une des formes suivantes :
	 * - MethodInvocation
	 * - SuperMethodInvocation
	 * - ClassInstanceCreation
	 * - ConstructorInvocation
	 * - SuperConstructorInvocation
	 */

	// invocation de méthode
	public boolean visit(MethodInvocation node) {
		nomMethode = node.getName().getIdentifier();
		List<?> arguments = node.arguments();
		IMethodBinding methodBinding = node.resolveMethodBinding();

		visitAnyMethodInvocation(arguments, methodBinding);

		return true;
	}

	// invocation de super méthodes
	public boolean visit(SuperMethodInvocation node) {
		nomMethode = node.getName().getIdentifier();
		List<?> arguments = node.arguments();
		IMethodBinding methodBinding = node.resolveMethodBinding();

		visitAnyMethodInvocation(arguments, methodBinding);

		return true;
	}

	// instanciation de classe avec 'new'
	public boolean visit(ClassInstanceCreation node) {
		nomMethode = node.getType().toString();
		List<?> arguments = node.arguments();
		IMethodBinding methodBinding = node.resolveConstructorBinding();

		visitAnyMethodInvocation(arguments, methodBinding);

		return true;
	}

	// invocation de constructeur
	public boolean visit(ConstructorInvocation node) {
		List<?> arguments = node.arguments();
		IMethodBinding methodBinding = node.resolveConstructorBinding();
		if (methodBinding != null) {
			nomMethode = methodBinding.getName();
		} else {
			nomMethode = "this";
		}

		visitAnyMethodInvocation(arguments, methodBinding);

		return true;
	}

	// invocation de super constructeur
	public boolean visit(SuperConstructorInvocation node) {
		List<?> arguments = node.arguments();
		IMethodBinding methodBinding = node.resolveConstructorBinding();
		if (methodBinding != null) {
			nomMethode = methodBinding.getName();
		} else {
			nomMethode = "super";
		}

		visitAnyMethodInvocation(arguments, methodBinding);

		return true;
	}

	/*
	 * =============================================================================
	 * Utilitaires
	 * =============================================================================
	 */

	// visite une invocation de méthode ou de super méthode
	private void visitAnyMethodInvocation(List<?> arguments, IMethodBinding methodBinding) {
		// calcul de la signature de la méthode invoquée
		nomMethode += '(';
		for (int i = 0; i < arguments.size(); i++) {
			// récupération du type de chaque argument
			Expression exprArg = (Expression) arguments.get(i);
			ITypeBinding typeBinding = exprArg.resolveTypeBinding();

			// si le type ne peut pas être trouvé, il est nommé '?' par défaut
			nomMethode += typeBinding != null ? typeBinding.getName() : '?';

			if (i + 1 < arguments.size()) {
				nomMethode += ',';
			}
		}
		nomMethode += ')';

		// récupération de la classe et du package du receveur
		String nomClasseDst = "?";
		String nomPackageDst = "?";

		if (methodBinding != null) {
			ITypeBinding typeBinding = methodBinding.getDeclaringClass();
			if (typeBinding != null) {
				nomClasseDst = typeBinding.getName();
				nomPackageDst = typeBinding.getPackage().getName();
			}
		}

		// ajout du noeud au graphe
		graphiste.ajouterNoeudDestination(nomPackageDst, nomClasseDst, nomMethode);
	}
}
