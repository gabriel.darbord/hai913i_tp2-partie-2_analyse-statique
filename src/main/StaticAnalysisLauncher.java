package main;

import ui.CommandLineInterface;
import ui.GraphicalUserInterface;

public class StaticAnalysisLauncher {

	public static void main(String[] args) {
		// pas d'arguments -> mode GUI
		if (args.length == 0) {
			new GraphicalUserInterface();
			return;
		}

		// sinon -> mode CLI
		// passage du chemin du projet à analyser en argument
		// ainsi que X pour sélectionner les classes ayant plus de X méthodes
		if (args.length != 2) {
			System.out.println("Mauvais arguments, attendu : <projectPath> <X>\n"
					+ "X est un entier utilisé pour sélectionner les classes ayant plus de X méthodes");
			return;
		}

		// récupération des arguments
		String projectPath = args[0];
		int X = Integer.parseInt(args[1]);

		new CommandLineInterface(projectPath, X);
	}

}
