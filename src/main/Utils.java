package main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NavigableMap;
import java.util.SortedMap;

public class Utils {

	// range un élément dans la collection de listes
	public static <K, V> void classifyElement(SortedMap<K, ArrayList<V>> rangs, K cle, V element) {
		ArrayList<V> elements = rangs.get(cle);

		if (elements == null) {
			elements = new ArrayList<>();
			rangs.put(cle, elements);
		}

		elements.add(element);
	}

	// retourne les K éléments les plus haut classés de la collection de listes
	public static <K, V> ArrayList<V> getTopK(NavigableMap<K, ArrayList<V>> rangs, int topK) {
		ArrayList<V> resultat = new ArrayList<>();

		// itération sur les éléments du rang le plus haut au plus bas
		for (List<V> elements : rangs.descendingMap().values()) {
			// cpt correspond au nombre d'élements à ajouter au résultat
			int cpt = topK - resultat.size();

			if (elements.size() > cpt) {
				resultat.addAll(elements.subList(0, cpt));
				break;
			}

			resultat.addAll(elements);
			if (resultat.size() == topK)
				break;
		}

		return resultat;
	}

	// calcule l'intersection entre deux listes
	public static <T> ArrayList<T> intersection(List<T> gauche, List<T> droite) {
		HashSet<T> resultat = new HashSet<>(gauche);

		resultat.retainAll(droite);

		return new ArrayList<>(resultat);
	}
}
