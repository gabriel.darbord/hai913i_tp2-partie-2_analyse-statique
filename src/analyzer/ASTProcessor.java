package analyzer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;
import java.util.function.Function;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import graphers.GraphvizDotGrapher;
import graphers.GraphvizOsageGrapher;
import graphers.IGrapher;
import main.Utils;
import visitors.CallGraphBuilder;
import visitors.MethodDeclarationVisitor;
import visitors.PackageDeclarationVisitor;
import visitors.TypeDeclarationVisitor;
import visitors.VariableDeclarationFragmentVisitor;

public class ASTProcessor {

	// ASTs obtenus par le Parser
	private String projectId;
	private ArrayList<CompilationUnit> asts;

	// résultats des visiteurs
	private List<TypeDeclaration> types;
	private List<PackageDeclaration> packages;
	private List<MethodDeclaration> methodes;
	private List<VariableDeclarationFragment> variables;

	// 10% du nombre de classes, calculés lors de l'analyse
	private int nbTopClasses;

	// nombre de lignes du projet analysé
	private int nbLignes;

	/*
	 * =============================================================================
	 * === Analyse
	 * =============================================================================
	 */

	// analyse le projet, conserve les infos obtenues et construit le graphe d'appel
	// retourne le nombre de lignes de l'application
	public void analyser(String projectPath) throws IOException {
		// le parser produit les AST des fichiers du projet
		Parser parser = new Parser(projectPath);
		asts = parser.getASTs();
		nbLignes = parser.getNbLignes();

		// instanciation des visiteurs
		TypeDeclarationVisitor tdv = new TypeDeclarationVisitor();
		PackageDeclarationVisitor pdv = new PackageDeclarationVisitor();
		MethodDeclarationVisitor mdv = new MethodDeclarationVisitor();
		VariableDeclarationFragmentVisitor vdv = new VariableDeclarationFragmentVisitor();

		// analyse des ASTs avec les visiteurs
		for (CompilationUnit ast : asts) {
			ast.accept(tdv);
			ast.accept(pdv);
			ast.accept(mdv);
			ast.accept(vdv);
		}

		// récupération des informations
		types = tdv.getTypes();
		packages = pdv.getPackages();
		methodes = mdv.getMethods();
		variables = vdv.getVariables();

		// combien de classes font 10% du nombre de classes
		nbTopClasses = (int) Math.ceil(types.size() * 0.1);

		// l'id correspond au du chemin absolu du projet analysé avec un pseudo-checksum
		long checksum = nbLignes * (types.size() + methodes.size() + packages.size() + variables.size() + 1);
		projectId = projectPath.replaceAll("[" + File.separator + ":. ]", "_") + "_" + checksum;
	}

	/*
	 * =============================================================================
	 * === Métriques
	 * =============================================================================
	 */

	// 1. Nombre de classes de l’application.
	public int nbClasses() {
		return types.size();
	}

	public int nbLignes() {
		return nbLignes;
	}

	// 3. Nombre total de méthodes de l’application.
	public int nbMethodes() {
		return methodes.size();
	}

	// 4. Nombre total de packages de l’application.
	public int nbPackages() {
		HashSet<String> nomsPackages = new HashSet<>();

		for (PackageDeclaration p : packages) {
			nomsPackages.add(p.getName().getFullyQualifiedName());
		}

		return nomsPackages.size();
	}

	// 5. Nombre moyen de méthodes par classe.
	public int moyenneMethodesParClasse() {
		if (types.isEmpty())
			return 0;

		return methodes.size() / types.size();
	}

	// 6. Nombre moyen de lignes de code par méthode.
	public int moyenneLignesParMethode() {
		if (methodes.isEmpty())
			return 0;

		int nbLignes = 0;

		for (MethodDeclaration methode : methodes) {
			nbLignes += methodLinesNumber(methode);
		}

		return nbLignes / methodes.size();
	}

	// 7. Nombre moyen d’attributs par classe.
	public int moyenneAttributsParClasse() {
		if (types.isEmpty())
			return 0;

		int nbAttributs = 0;

		for (VariableDeclarationFragment variable : variables) {
			if (variable.getParent().getNodeType() == ASTNode.FIELD_DECLARATION) {
				nbAttributs++;
			}
		}

		return nbAttributs / types.size();
	}

	// 8. les 10% des classes qui possèdent le plus grand nombre de méthodes
	public ArrayList<String> topClassesParNbMethodes() {
		return topClassesParNbY(TypeDeclaration::getMethods);
	}

	// 9. les 10% des classes qui possèdent le plus grand nombre d’attributs
	public ArrayList<String> topClassesParNbAttributs() {
		return topClassesParNbY(TypeDeclaration::getFields);
	}

	// 11. les classes qui possèdent plus de X méthodes
	public ArrayList<String> classesAvecXMethodes(int X) {
		ArrayList<String> resultat = new ArrayList<>();

		for (TypeDeclaration type : types) {
			if (type.getMethods().length > X) {
				resultat.add(type.getName().toString());
			}
		}

		return resultat;
	}

	// 12. Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code
	// (par classe).
	public HashMap<String, ArrayList<String>> topMethodesParNbLignes() {
		HashMap<String, ArrayList<String>> resultat = new HashMap<>();

		for (TypeDeclaration type : types) { // par classe
			TreeMap<Integer, ArrayList<String>> rangs = new TreeMap<>();
			MethodDeclaration[] methodes = type.getMethods();

			for (int i = 0; i < methodes.length; i++) {
				MethodDeclaration methode = methodes[i];

				// classement de la méthode
				int nbLignes = methodLinesNumber(methode);
				String nom = methode.getName().toString();
				Utils.classifyElement(rangs, nbLignes, nom);
			}

			// combien de méthodes font 10% du nombre de méthodes de la classe
			int nbTopMethodes = (int) Math.ceil(methodes.length * 0.1);

			// sélection du top 10% des méthodes
			ArrayList<String> topMethodes = Utils.getTopK(rangs, nbTopMethodes);
			String nomClasse = type.getName().getFullyQualifiedName();
			resultat.put(nomClasse, topMethodes);
		}

		return resultat;
	}

	// 13. Le nombre maximal de paramètres par rapport à toutes les méthodes de
	// l’application.
	public int nbMaxParametres() {
		int max = 0;

		for (MethodDeclaration methode : methodes) {
			max = Math.max(max, methode.parameters().size());
		}

		return max;
	}

	/*
	 * =============================================================================
	 * === Graphe d'appel
	 * =============================================================================
	 */

	// génère le graphe d'appel en utilisant graphviz et le moteur :
	// - dot pour un graphe hiérarchique des appels de méthode
	// - osage pour une vue cartographique du projet
	// le 'format' sera celui du fichier dont le chemin est retourné
	public String grapheAppel(boolean hierarchie, String format) {
		// choix du graphiste
		IGrapher graphiste;
		if (hierarchie) {
			graphiste = new GraphvizDotGrapher(projectId, format);
		} else {
			graphiste = new GraphvizOsageGrapher(projectId, format);
		}

		// vérification si le graphe a déjà été fait
		String graphe = graphiste.estDejaFait();
		if (graphe != null) {
			return graphe;
		}

		// visite du projet par le builder
		CallGraphBuilder graphBuilder = new CallGraphBuilder(graphiste);
		for (CompilationUnit ast : asts) {
			ast.accept(graphBuilder);
		}

		return graphiste.terminerGraphe();
	}

	/*
	 * =============================================================================
	 * === Utilitaires
	 * =============================================================================
	 */

	// le Y dont le nombre est à évaluer est obtenu avec l'appel de méthode 'func'
	private ArrayList<String> topClassesParNbY(Function<TypeDeclaration, Object[]> func) {
		TreeMap<Integer, ArrayList<String>> rangs = new TreeMap<>();

		for (TypeDeclaration type : types) {
			int nb = func.apply(type).length;
			String nom = type.getName().toString();
			Utils.classifyElement(rangs, nb, nom);
		}

		return Utils.getTopK(rangs, nbTopClasses);

//		// avec Stream pour le plaisir, mais moins performant
//		return types.stream()
//				.sorted((x, y) -> func.apply(y).length - func.apply(x).length)
//				.limit(nbTopClasses)
//				.map((x) -> x.getName().getFullyQualifiedName())
//				.collect(Collectors.toList());
	}

	// compte le nombre de lignes d'une méthode
	private static int methodLinesNumber(MethodDeclaration methode) {
		CompilationUnit ast = (CompilationUnit) methode.getRoot();

		int position = methode.getStartPosition();
		int debut = ast.getLineNumber(position);
		int fin = ast.getLineNumber(position + methode.getLength());

		return fin - debut + 1;
	}
}
