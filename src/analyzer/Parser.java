package analyzer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

public class Parser {
	private static final String jrePath = "/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/rt.jar";

	private final ArrayList<CompilationUnit> asts = new ArrayList<>();
	private final String projectSourcePath;
	private int nbLignes = 0; // calculé à la construction

	public Parser(String projectPath) throws IOException {
		projectSourcePath = projectPath + "/src";

		// read java files
		final File folder = new File(projectSourcePath);
		if (!folder.exists()) {
			throw new FileNotFoundException("Project source path does not exist: " + projectSourcePath);
		}
		ArrayList<File> javaFiles = listJavaFilesForFolder(folder);

		parse(javaFiles);
	}

	public ArrayList<CompilationUnit> getASTs() {
		return asts;
	}

	public int getNbLignes() {
		return nbLignes;
	}

	// create AST
	private void parse(ArrayList<File> javaFiles) throws IOException {
		ASTParser parser = ASTParser.newParser(AST.JLS4); // java +1.6
		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		Map<?, ?> options = JavaCore.getOptions();
		parser.setCompilerOptions(options);

		String[] classpath = { jrePath };
		String[] sources = { projectSourcePath };

		for (File javaFile : javaFiles) {
			// configuration du parseur qui est remis à zéro après 'createAST()'
			parser.setEnvironment(classpath, sources, new String[] { "UTF-8" }, true);
			parser.setUnitName(javaFile.getAbsolutePath());
			parser.setResolveBindings(true);

			// chargement du contenu du fichier java
			String content = FileUtils.readFileToString(javaFile);
			parser.setSource(content.toCharArray());

			// compte du nombre de lignes
			nbLignes += content.lines().count();

			// création et ajout de l'ast
			CompilationUnit ast = (CompilationUnit) parser.createAST(null);
			asts.add(ast);
		}
	}

	// read all java files from specific folder
	private static ArrayList<File> listJavaFilesForFolder(final File folder) {
		ArrayList<File> javaFiles = new ArrayList<File>();

		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				javaFiles.addAll(listJavaFilesForFolder(fileEntry));
			} else if (fileEntry.getName().endsWith(".java")) {
				javaFiles.add(fileEntry);
			}
		}

		return javaFiles;
	}
}
