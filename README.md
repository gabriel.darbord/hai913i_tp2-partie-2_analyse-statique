# HAI913I_TP2 Partie 2_Analyse statique

Application qui permet d’analyser le code source d’une application orientée objet (donnée comme paramètre), et de calculer des statistiques et construire un graphe d'appels.

## Utilisation
Pour utiliser le mode :
- CLI, donner en paramètres le chemin du projet à analyser et X un nombre entier.  
- GUI, ne pas donner de paramètres, cliquer sur le bouton en haut de la fenêtre pour sélectionner un projet à analyser.
